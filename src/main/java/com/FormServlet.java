package com;

import com.sun.mail.smtp.SMTPTransport;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.Properties;


@WebServlet("/zapytanie")
public class FormServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=UTF-8");
        String name = req.getParameter("name");

        String email = req.getParameter("email");

        String phone = req.getParameter("phone");

        if (phone.equals("") && email.equals("")) {


            req.setAttribute("noContact", true);

            req.getRequestDispatcher("/index.jsp").forward(req, resp);

            return;

        } else {

            String temat = req.getParameter("temat");
            String data = req.getParameter("data");
            String message = req.getParameter("message");

            Zapytanie newZapytanie = new Zapytanie(name, email, phone, temat, data, message);


            String SMTP_SERVER = "smtp.gmail.com";
            String USERNAME = "mailtotest123321";
            String PASSWORD = "haslodomailadotestu";

            String EMAIL_FROM = "haslodomailadotestu@gmail.com";
            String EMAIL_TO = "Jazdaslubna@gmail.com";
            String EMAIL_TO_CC = "";

            String EMAIL_SUBJECT = "--> Masz nowe zapytanie dot. wynajmu Merolka <--";

            String EMAIL_TEXT = "\n\nImie i nazwisko: " + newZapytanie.getName() +
                    "\nE-mail: " + newZapytanie.getEmail() +
                    "\nNumer telefonu: " + newZapytanie.getPhone() +
                    "\nTemat: " + newZapytanie.getTemat() +
                    "\nData rezerwacji:" + newZapytanie.getData() +
                    "\nTreść wiadomości: " + newZapytanie.getMessage() +
                    "\n\n";


            Properties prop = System.getProperties();
            prop.put("mail.smtp.host", "smtp.gmail.com");

            prop.put("mail.smtp.ssl.trust", "smtp.gmail.com");
            prop.put("mail.smtp.port", "587");
            prop.put("mail.smtp.auth", "true");
            prop.put("mail.smtp.starttls.enable", "true");


            Session session = Session.getInstance(prop, null);
            Message msg = new MimeMessage(session);


            try {

                msg.setFrom(new InternetAddress(EMAIL_FROM));

                msg.setRecipients(Message.RecipientType.TO,
                        InternetAddress.parse(EMAIL_TO, false));

                msg.setRecipients(Message.RecipientType.CC,
                        InternetAddress.parse(EMAIL_TO_CC, false));

                msg.setSubject(EMAIL_SUBJECT);

                msg.setText(EMAIL_TEXT);

                msg.setSentDate(new Date());

                SMTPTransport t = (SMTPTransport) session.getTransport("smtp");

                t.connect(SMTP_SERVER, USERNAME, PASSWORD);

                t.sendMessage(msg, msg.getAllRecipients());

                System.out.println("Response: " + t.getLastServerResponse());

                t.close();

                req.setAttribute("isSended", true);
                RequestDispatcher requestDispatcher = req.getRequestDispatcher("/index.jsp");
                requestDispatcher.forward(req, resp);

            } catch (MessagingException e) {
                e.printStackTrace();
            }

        }
    }
}


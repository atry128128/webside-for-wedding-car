package com;


public class Zapytanie {
    private String name;
    private String email;
    private String phone;
    private String temat;
    private String data;
    private String message;

    public Zapytanie(String name, String email, String phone, String temat, String data, String message) {
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.temat = temat;
        this.data = data;
        this.message = message;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getTemat() {
        return temat;
    }

    public void setTemat(String temat) {
        this.temat = temat;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

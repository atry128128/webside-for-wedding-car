<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="pl-PL">
<head>
    <meta charset="UTF-8">
    <title>Jazda ślubna</title>
    <meta charset="UTF-8">
    <title>Main page</title>

    <link rel="icon" href="img/car.png" type="image/icon type">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">


    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
          rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Marcellus+SC&display=swap" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/grayscale.min.css" rel="stylesheet">

    <!--  glowny napis na main page-->
    <link href="https://fonts.googleapis.com/css?family=Vollkorn+SC&display=swap" rel="stylesheet">
    <%--Tekst mercedes to wlasnie to--%>
    <link href="https://fonts.googleapis.com/css?family=Unna&display=swap" rel="stylesheet">


</head>

<body id="page-top">

<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">

        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
                aria-label="Toggle navigation">
            Menu
            <i class="fas fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="#projects">O samochodzie</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="#gallery">Galeria zdjęć</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="#signup">Oferta</a>
                </li>
                </li>
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="#askUs">Złóż zapytanie</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="#contact">Kontakt</a>
                </li>
            </ul>
        </div>
    </div>
</nav>

<!-- Header -->
<header class="masthead">
    <div class="container d-flex h-100 align-items-center">
        <div style="width: auto !important;" class="mx-auto text-center">
            <h2 class="mx-auto my-0 text-uppercase"
                style=" font-family: 'Vollkorn SC', serif;color: white; font-weight: revert; font-size: 350% ">
                Mercedes CLS AMG</h2>
            <br>
            <h3 class="text-white-50 mx-auto mt-2 mb-5">Jazda ślubna</h3>

        </div>
    </div>
</header>

<!-- About Section -->
<section id="about" class="about-section text-center">
    <div class="container">
        <div  style="padding-left: 30px" class="row">
            <div class="col-lg-8 mx-auto">
                <!--          <h2 class="text-white mb-4">Built with Bootstrap 4</h2>-->
                <p class="text-white-50">Dynamiczne coupe połączone z dużą i
                    komfortową limuzyną,<br> przy
                    jednoczesnym zachowaniu
                    drapieżnego charakteru, <br>które sprawi,
                    że poczujecie się wyjątkowo
                <h1 style="color: #b3d7ff !important; font-family: 'Unna', serif;" class="text-white mb-4">To wyróżnia Mercedesa CLS
                    AMG</h1>
                </p>
            </div>
        </div>
        <img src="img/foto1.jpg" class="img-fluid" alt="">
    </div>
</section>

<!-- Projects Section -->
<%@ include file="index.html" %>
<!-- Signup Section -->
<section class="signup-section">
    <div id="signup" class="container">
        <div class="row">
            <div style="width: 100% !important; margin: 0 !important;" class="col-md-12 col-lg-12 mx-auto text-center">


                <br><br>
                <h1 style="text-align: center;padding-left: 30px" class="text-white mb-5">Oferta</h1>
                <%--                <hr style="background-color: white; width: 40%; height: 3px;padding-left: 80px; margin-right: 34px; margin-top: 0 !important;">--%>


                <div style="width: 100% !important; margin: 0 !important;padding-left: 30px" class="row">
                    <div class="col-lg-4 col-md-4 text-center">
                        <div class="mt-5">
                            <i style="color: #f8d7da !important;" class="fas fa-4x fa-gem text-primary mb-4"></i>
                            <h3 style="color: white" class="h4 mb-2">Podstawowy pakiet:</h3><br>
                            <p style="color: white !important;" class="text-muted mb-0">-odebranie Panny Młodej oraz
                                Pana
                                Młodego, <br>
                                –przejazd do kościoła lub Urzędu Stanu Cywilnego, <br>
                                –dojazd do sali weselnej</p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 text-center">
                        <div class="mt-5">
                            <i style="color: #f8d7da !important;" class="fas fa-4x fa-heart text-primary mb-4"></i>

                            <h3 style="color: white" class="h4 mb-2">Usługa zawiera:</h3><br>
                            <p style="color: white!important;" class="text-muted mb-0">–szykownie ubranego,
                                sympatycznego
                                kierowcę,<br>
                                –wynajem 4 godzinny z limitem do 100 km,<br>
                                –ozdobienie samochodu (dodatkowe życzenia proszę zamieścić w uwagach w formularzu)</p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 text-center">
                        <div class="mt-5">
                            <i style="color: #f8d7da !important;" class="fas fa-4x fa-gem text-primary mb-4"></i>
                            <h3 style="color: white" class="h4 mb-2">Cennik:</h3><br>
                            <p style="color: white!important;" class="text-muted mb-0">Ceny ustalane są indywidualnie w
                                zależności od lokalizacji,
                                długości wynajmu oraz dodatkowych wymagań.
                                Cena orientacyjna pakietu zamieszczonego w pakiecie podstawowym:
                                Od 599 zł na terenie Wrocławia W celu uzyskania indywidualnej wyceny należy wypełnić
                                formularz.</p>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
</section>

<!--Ask us -->

<section style="background-color: #191919 ; color: #f5c6cb" class="page-section" name="askUs" id="askUs">

    <div style="background-color: #191919 ; color: #f5c6cb" class="container">
        <div class="row" style="padding-left: 30px">
            <div class="col-lg-12 text-center">
                <i class="far fa-paper-plane fa-2x mb-2 text-white"></i><br>
                <h2 class="section-heading text-uppercase">Zadaj nam pytanie</h2><br><br>

            </div>
        </div>
        <div class="row" style="padding-left: 30px;margin-right: 0;margin-left: 0">
            <div class="col-lg-12">
                <form method="post" action="zapytanie#askUs" id="contactForm" name="sentMessage">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input class="form-control" name="name" type="text" placeholder="Imię i nazwisko">


                                <input type="hidden" name="podstrona" value="#askUs">


                            </div>
                            <div class="form-group">
                                <input class="form-control" name="email" type="email" placeholder="E-mail">

                            </div>
                            <div class="form-group">
                                <input class="form-control" name="phone" type="number" placeholder="Nr telefonu">
                            </div>


                            <div class="form-group">
                                <input class="form-control" list="temat" name="temat" placeholder="Pytanie dotyczy">
                                <datalist id="temat">
                                    <option value="wyceny uslugi">
                                    <option value="rezerwacji">
                                    <option value="dostępnych terminów">
                                    <option value="inne">
                                </datalist>
                                <p class="help-block text-danger"></p>
                            </div>
                            <div class="form-group">
                                <input class="form-control" name="data" type="text"
                                       placeholder="Data rezerwacji (dzień-miesiac-rok)">

                            </div>
                        </div>


                        <div class="col-md-6">
                            <div class="form-group">
                                <textarea rows="10" class="form-control" name="message"
                                          placeholder="Treść wiadomości"></textarea>

                            </div>
                        </div>

                        <div class="col-md-12">
                            <%
                                if (request.getAttribute("noContact") != null
                                        && request.getAttribute("isSended") == null) {
                            %>
                            <div class="form-group" style="color: red">
                                <div class="form-control"
                                     style="color: red; font-size:15px; text-align: center; height: auto">
                                    <b> Podaj nr telefonu lub email, żebyśmy mogli się z Tobą skontaktować :)</b>

                                </div>
                            </div>
                            <%}%>
                        </div>


                        <div class="col-md-12">
                            <%if (request.getAttribute("isSended") != null) { %>
                            <div class="form-group" style="color: red">
                                <div class="form-control"
                                     style="color: red; font-size:15px; text-align: center; height: auto">
                                    <b> Dziękujemy. Wiadomość została wysłana. Wkrótce skontaktujemy się z Tobą.</b>
                                </div>
                            </div>
                            <%}%>
                        </div>


                        <div class="clearfix"></div>
                        <div class="col-lg-12 text-center">
                            <div id="success"></div>
                            <button id="sendMessageButton" class="btn btn-primary btn-xl text-uppercase" type="submit">
                                Wyślij zapytanie
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<!-- Contact Section -->
<section class="contact-section background-contact" id="contact">
    <div class="container">

        <div  style="padding-left: 30px" class="row">

            <div class="col-md-4 mb-3 mb-md-0">
                <div class="card py-4 h-100">
                    <div class="card-body text-center">
                        <i class="fas fa-map-marked-alt text-primary mb-2"></i>
                        <h4 class="text-uppercase m-0">Adres</h4>
                        <hr style="background-color: #CC9933 !important;" class="my-4">
                        <div class="small text-black-50">Nowy Dwór<br>54-440 Wrocław</div>
                    </div>
                </div>
            </div>

            <div class="col-md-4 mb-3 mb-md-0">
                <div class="card py-4 h-100">
                    <div class="card-body text-center">
                        <i class="fas fa-envelope text-primary mb-2"></i>
                        <h4 class="text-uppercase m-0">E-mail</h4>
                        <hr style="background-color: #CC9933" class="my-4">
                        <div class="small text-black-50">
                            <a href="#">Jazdaslubna@gmail.com</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4 mb-3 mb-md-0">
                <div class="card py-4 h-100">
                    <div class="card-body text-center">
                        <i class="fas fa-mobile-alt text-primary mb-2"></i>
                        <h4 class="text-uppercase m-0">Nr telefonu</h4>
                        <hr style="background-color: #CC9933" class="my-4">
                        <div class="small text-black-50">786-214-993</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="social d-flex justify-content-center">

            <a href="http://www.facebook.pl" class="mx-3">
                <i class="fab fa-facebook-f"></i>
            </a>
            <a href="http://www.instagram.com" class="mx-3">

                <i class="fab fa-instagram"></i>
            </a>
        </div>

    </div>
</section>

<!-- Footer -->
<footer class="background-footer small text-center text-black-50">
    <div class="container">
        Website created by <b>Joanna Kwaśniewska</b><br>537 903 635</br>joanna.kwasniewska71@gmail.com
    </div>
</footer>

<!-- Bootstrap core JavaScript -->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Plugin JavaScript -->
<script src="vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Custom scripts for this template -->
<script src="js/grayscale.min.js"></script>

</body>


</html>